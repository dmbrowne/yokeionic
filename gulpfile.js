var gulp          = require('gulp');
var gutil         = require('gulp-util');
var bower         = require('bower');
var concat        = require('gulp-concat');
var sass          = require('gulp-sass');
var minifyCss     = require('gulp-minify-css');
var rename        = require('gulp-rename');
var sh            = require('shelljs');
var jshint        = require('gulp-jshint')
var jshintStylish = require('jshint-stylish')
var html2js       = require('gulp-html2js')
var paths         = require('./gulp.config')

gulp.task('install', ['git-check'], bowerInstall)
gulp.task('git-check', checkForGit)

gulp.task('sass', sassBuild)

gulp.task('jshint', lint)
gulp.task('appjs', ['jshint'], applicationJs)
gulp.task('vendorJs', libJs)

gulp.task('templateCache', appTemplateCache)

gulp.task('assets: fonts', vendorFonts)
gulp.task('assets: images', imagesCopy)

gulp.task('default', ['sass', 'appjs', 'vendorJs', 'templateCache', 'assets: images', 'assets: fonts'])

gulp.task('watch', function() {
  gulp.watch(paths.sass.files, ['sass']);
  gulp.watch(paths.js.files, ['appjs']);
  gulp.watch(paths.apptpl, ['templateCache']);
  gulp.watch(paths.images.src, ['assets: images']);
});





/* ==================
 * =========
 * Ionic Tasks
 * ====
 */

function bowerInstall() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
}

function checkForGit(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
}


/* ==================
 * =========
 * Sass/Scss Tasks
 * ====
 */
      function sassBuild(done) {
        gulp.src(paths.sass.mainFiles)
          .pipe(sass({
            errLogToConsole: true
          }))
          .pipe(gulp.dest(paths.sass.dest))
          .pipe(minifyCss({
            keepSpecialComments: 0
          }))
          .pipe(rename({ extname: '.min.css' }))
          .pipe(gulp.dest(paths.sass.dest))
          .on('end', done);
      }

/* ==================
 * =========
 * Javascript Tasks
 * ====
 */

      function lint() {
          return gulp.src(paths.js.files)
              .pipe(jshint({
                  asi: true
              }))
              .pipe(jshint.reporter('default'));
      }

      function applicationJs() {
          return gulp.src(paths.js.files)
              .pipe(concat('app.js'))
              .pipe(gulp.dest(paths.js.dest));
      }

      function libJs(){
          if(paths.vendor.js.length <= 0) return

          return gulp.src(paths.vendor.js)
              .pipe(concat('lib.js'))
              .pipe(gulp.dest(paths.js.dest))
      }



/* ==============================
 * ====================
 * Template Cache
 * ======
 * ===
 */
    function appTemplateCache(){
      return gulp.src(paths.apptpl)
        .pipe(html2js({
          outputModuleName: 'appTemplates',
          useStrict: true,
          base: paths.apptplBase
        }))
        .pipe(concat('appPartials.tpl.js'))
        .pipe( gulp.dest(paths.js.dest) )
    }



/* ==============================
 * ====================
 * assets
 * ======
 * ===
 */

    function imagesCopy(){
        return gulp.src(paths.images.src)
            .pipe(gulp.dest(paths.images.dest))
    }

    function vendorFonts(){
        // if(paths.vendor.fonts.length <= 0) return
        gulp.src(paths.fonts.src)
            .pipe(gulp.dest(paths.fonts.dest))

        gulp.src(paths.vendor.fonts)
            .pipe(gulp.dest(paths.fonts.dest))
    }
