var gulpConfig = {
	sass: {
		mainFiles: ['./build/scss/style.scss'],
		files: './build/scss/**/*.scss',
		dest: './www/css'
	},

	js: {
		files: './build/js/**/*.js',
		dest:  './www/js'
	},

	vendor:{
		js: [
			'./bower_components/lodash/lodash.js',
			'./bower_components/ionic/js/ionic.bundle.js',
			'./bower_components/angular-resource/angular-resource.js',
		],
		css: [],
		fonts:[
			'./bower_components/ionic/fonts/**/*'
		],
	},

	fonts: {
		src: './build/fonts/**/*',
		dest: './www/fonts'
	},

	images: {
		src: './build/img/**/*',
		dest: './www/img'
	},

	apptpl: './build/templates/**/*.html',
	apptplBase: './build/templates'
}

module.exports = gulpConfig;