angular
	.module('chat')
	.config(chatDetailConfiguration)

function chatDetailConfiguration($stateProvider) {
	$stateProvider.state('tab.chats.detail', {
		url: '/:private/:chatId',
		params: {
			private: '1'// '0' for non private
		},
		views: {
			'tab-chats@': {
				templateUrl: 'chat/chat-detail.html',
				controller: 'ChatDetailCtrl',
			}
		},
		resolve: {
			isPrivateChat: function($stateParams){
				return parseInt($stateParams.private, 10, 2)
			},

			chatId: function($stateParams){
				return $stateParams.chatId
			},

			messages: function(PrivateConversationService, isPrivateChat, chatId){
				var msgs;

				if(isPrivateChat){
					msgs = PrivateConversationService.convoById().get({
						id: chatId
					}).$promise
				}else{
					//
				}

				return msgs.then(function(response){
					return response.messages
				})
			}
		}
    })
}
