angular.module('chat')
		.config(chatRootConfiguration)
		.run(function($rootScope){
			$rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error){
				console.log(error)
			})
		})


function chatRootConfiguration($stateProvider) {
	$stateProvider.state('tab.chats', {
		url: '/chats',
		abstract: true,
		views: {
	    	'tab-chats': {
	    		// template: '<div ui-view />',
	    		templateUrl: 'chat/tab-chat.html',
	    	}
		},
		resolve: {

			_: '_',

			CurrentUserService: 'CurrentUserService',

			SocketIO: 'SocketIO',

			connection: function(SocketIO){
				return SocketIO.connect('/chat')
			},

			user: function(CurrentUserService){
				return CurrentUserService.user()
			},

			allPartners: function(user){
				return user.Partner
			},

			activePartner: function(allPartners, _){
				return _.filter(allPartners, { UserRelationship: { deleted_at: null } })[0]
			}
		}
	})
}
