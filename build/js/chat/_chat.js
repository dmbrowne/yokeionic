angular.module('chat', [
	'ionic',
	'lodashService',
	'CurrentUserServiceModule',
	'SocketService',
	'api'
])
