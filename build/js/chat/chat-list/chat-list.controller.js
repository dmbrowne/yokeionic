angular
	.module('chat')
	.controller('ChatsListCtrl', ChatListController)

ChatListController.$inject = ['$state', 'user', 'activePartner','partnerConversation', 'PrivateConversationService'];
function ChatListController($state, user, activePartner, partnerConversation, PrivateConversationService){
	var self          = this
	this.user         = user
	this.partner      = activePartner
	this.partnerConvo = partnerConversation.null ? undefined : partnerConversation

	this.createNewPartnerThread = function(){
		var createObj = { id: self.partner.UserRelationship.relationship_id }

		PrivateConversationService
			.conversationByRelationshipId()
			.save(createObj, {})
			.$promise
			.then(function(newThread){
				$state.go('tab.chat.detail', {
					private: '1',
					chatId: newThread.id
				})
			})
	}
}
