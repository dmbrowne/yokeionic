angular
	.module('chat')
	.config(chatListConfiguration)


function chatListConfiguration($stateProvider) {
	$stateProvider.state('tab.chats.list', {
		url: '',
		views: {
	    	'chat-list': {
	    		templateUrl: 'chat/chat-list.html',
	    		controller: 'ChatsListCtrl as chatListCntl'
	    	}
		},
		resolve: {
			PrivateConversationService: 'PrivateConversationService',
			partnerConversation: function(PrivateConversationService, activePartner){
				return PrivateConversationService
					.conversationByRelationshipId()
					.get({
						id: activePartner.UserRelationship.relationship_id,
						limit: 1
					})
					.$promise
			}
		}
	})
}
