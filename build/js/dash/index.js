angular
	.module('dash', ['ionic'])
	.config(dashConfiguration)
	.controller('DashCtrl', function($scope) {})

function dashConfiguration($stateProvider) {
	$stateProvider.state('tab.dash', {
		url: '/dash',
		views: {
			'tab-dash': {
				templateUrl: 'dash/tab-dash.html',
				controller: 'DashCtrl'
			}
		}
	})
}