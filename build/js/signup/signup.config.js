angular
	.module('signup')
	.config(signupConfiguration)


signupConfiguration.$inject = ['$stateProvider']
function signupConfiguration($stateProvider){
	$stateProvider
		.state('signup', {
			url:          '/signup',
		    templateUrl:  'signup/signup-index.html',
		    controller:   function($ionicNavBarDelegate){
				$ionicNavBarDelegate.showBar(false)
			}
		})
		.state('signup-joint', {
			url:          '/signup-joint',
		    templateUrl:  'signup/joint-signup.html',
		    controller:   'SignupController as signupCtrl'
		})
		.state('signup-single', {
			url:          '/signup-single',
		    templateUrl:  'signup/single-signup.html',
		    controller:   'SignupController as signupCtrl'
		})
}