angular
	.module('signup')
	.controller('SignupController', signupController)

signupController.$inject = ['UserService', '$ionicPopup', '$scope', '$state', '$ionicNavBarDelegate'];
function signupController(UserService, $ionicPopup, $scope, $state, $ionicNavBarDelegate){
	var self = this
	$ionicNavBarDelegate.showBar(false)
	self.signUpFields = {}

	self.createAccount = function(form){
		if(!form.$valid) return;

		// remove after dev
		if(self.signUpFields.email === undefined || self.signUpFields.password === undefined){
			console.warn('user signupFields scopeController object as ng model to store input fields')
			return
		}

		signupUser = {
			email: self.signUpFields.email,
			password: self.signUpFields.password
		}

		if( self.signUpFields.partnerEmail )
			signupUser.partner = { email: self.signUpFields.partnerEmail }

		UserService.users
			.save(signupUser)
			.$promise
			.then(accountCreateResponse)
			.catch(errorResponse)
	}

	function accountCreateResponse(){
		$state.go('login')
	}

	function errorResponse(res){
		if(res.status === 422){
    		$scope.existingUsers = res.data.map(function(emailAddr){
    			var belongsTo;

    			if(emailAddr === self.signUpFields.email)
    				belongsTo = 'user'
    			else
    				belongsTo = 'partner'

    			return {
    				email: emailAddr,
    				belongsTo: belongsTo
    			}
    		})

    		$ionicPopup.alert({
				title: 'User Already Exists',
				templateUrl: 'signup/user-already-exists.html',
				scope: $scope
			})
		}
	}
}
