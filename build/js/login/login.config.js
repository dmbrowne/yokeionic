angular.module('login')
	.config(loginConfiguration)

loginConfiguration.$inject = ['$stateProvider']
function loginConfiguration($stateProvider){
	$stateProvider.state('login', {
		url:          '/login',
	    templateUrl:  'login.html',
	    controller:   'loginController as lgnCtrl',
	})
}