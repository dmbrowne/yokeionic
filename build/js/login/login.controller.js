angular.module('login')
	.controller('loginController', loginCntrlr)

loginCntrlr.$inject = [ '$state', 'UserService', '$ionicPopup', '$ionicNavBarDelegate', 'CurrentUserService' ]
function loginCntrlr ($state, UserService, $ionicPopup, $ionicNavBarDelegate, CurrentUserService){
	var self = this;
	$ionicNavBarDelegate.showBar(false)

	self.login = function(form){
		if(!form.$valid) return;
		UserService.getToken({
			email: form.email.$viewValue,
			password: form.password.$viewValue
		})
		.then(function(response){
			successHandler(response);  
		})
		.catch(errorHandler)
	}

	function successHandler(res){
		CurrentUserService.token(res.token)

		UserService.users
			.get({ user_id: res.user.id })
			.$promise
			.then(function(user){
				CurrentUserService.user(user)
				$state.go('tab.dash')
			})
	}

	function errorHandler(errorResponse){
		if ( errorResponse.status == 401 ){
			return authFail(errorResponse.data.message)
		}else{
			$ionicPopup.alert({
				title: 'An error occured',
				template: 'try again later'
			})
		}
	}

	function authFail(errMsgStr){
		if (errMsgStr == "user doesn't exist"){
			$ionicPopup.alert({
				title: 'User Does not exist',
				template: 'we cannot find a user with that email'
			})
		}

		if (errMsgStr == "incorrect password"){
			$ionicPopup.alert({
				title: 'Incorrect password',
				template: 'password seems to be incorrect'
			})
		}
	}
}