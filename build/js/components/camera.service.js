angular
	.module('cameraModule', [])
	.service('Camera', ionicCamera)

ionicCamera.$inject = ['$q']
function ionicCamera($q) {
	this.getPicture = function(options) {
		var q = $q.defer();

		navigator.camera.getPicture(
			function(result) {
				// Do any magic you need
				q.resolve(result);
			},
			function(err) {
				q.reject(err);
			},
			options
		)

		return q.promise;
	}
}
