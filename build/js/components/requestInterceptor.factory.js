angular
    .module('yokeTokenInterceptorFactoryModule', ['ngResource', 'CurrentUserServiceModule', ])
    .factory('TokenRequestInterceptor', TokenRequestInterceptor);


TokenRequestInterceptor.$inject = ['$q', 'CurrentUserService']
function TokenRequestInterceptor ($q, CurrentUserService){
    return {
        request: function(config) {
            config.headers = config.headers || {};
            var userToken  = CurrentUserService.token()

            if(userToken)
                config.headers.Authorization = 'Bearer ' + userToken

            return config;
        },

        response: function(response) {
            if(response.data === null)
                response.data = { null: true };

            /*if (response.status === 403 || response.status === 401) {
                // insert code to redirect to custom unauthorized page
            }*/
            return response || $q.when(response);
        }
    }
}

