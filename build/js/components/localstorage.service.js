angular
	.module('localstorageModule', [])
	.factory('$localstorage', localStore)

localStore.$inject = ['$window']
function localStore($window) {
	return {
		set: function(key, value) {
			$window.localStorage[key] = JSON.stringify(value);
		},
		get: function(key) {
			if( $window.localStorage[key] === undefined )
				return undefined
			else
				return JSON.parse($window.localStorage[key])
		}
	}
}