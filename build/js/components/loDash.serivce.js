
angular
	.module('lodashService', [])
	.factory('_', LoDash)

LoDash.$inject = ['$window']
function LoDash($window){
	var _ = $window._
	delete($window._)

	return ( _ )
}
