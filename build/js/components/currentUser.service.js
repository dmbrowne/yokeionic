
angular
	.module('CurrentUserServiceModule', ['localstorageModule', 'lodashService'])
	.factory('CurrentUserService', CurrentUserService)

CurrentUserService.$inject = ['$localstorage', '_']
function CurrentUserService($localstorage, _){
	var sessionUser = {}
	constructor()

	return {
		user: function(usr){
			if(usr){
				sessionUser.user = usr
				$localstorage.set('current-user', sessionUser)
			}

			return ( _.size( sessionUser.user ) ? sessionUser.user : undefined )
		},

		token: function(tken){
			if(tken){
				sessionUser.token = tken
				$localstorage.set('current-user', sessionUser)
			}

			return sessionUser.token
		},

		addUserDetails: function(newDetails){
			var user = sessionUser.user

			_.forEach(newDetails, function(val, key){
				user[key] = val
			})

			$localstorage.set('current-user', sessionUser)

			return sessionUser.user
		}
	}

	function constructor(){
		if( $localstorage.get('current-user') )
			sessionUser = $localstorage.get('current-user')
		else
			sessionUser = {}
	}
}
