angular
    .module('SocketService', ['CurrentUserServiceModule'])
    .service('SocketIO', SocketIOservice)

SocketIOservice.$inject = ['$rootScope', 'CurrentUserService', '$q'];
function SocketIOservice($rootScope, CurrentUserService, $q){
    var socketSrvc = this

    // Create socket object and connection. do not create with undefined on run so that the instance can be saved
    socketSrvc.connect = function(namespace){
        var deferred = $q.defer();
        namespace    = namespace || '/'

        if ( socketSrvc.socket && socketSrvc.socket.connected && socketSrvc.socket.nsp === namespace ) return

        /* == One way roundtrip: less secure ==*/
        /*socketSrvc.socket = window.io('http://localhost:3000' + namespace, {
            query: 'token=' + CurrentUserService.token(),
            forceNew: true
        })*/

        /* For oneway roundtrip */
        /*socketSrvc.on('connect', function(){
            socketSrvc.authenticate()
                .then(deferred.resolve)
                .catch(deferred.reject)
        })*/
        
        /* == Two way roundtrip: more secure ==*/
        socketSrvc.socket = window.io('http://localhost:3000' + namespace)

        /* == Two way roundtrip: more secure ==*/
        socketSrvc.on('connect', function(){
            socketSrvc.on('authenticated', function(){
                socketSrvc.isAuthenticated = true
                deferred.resolve()
            })

            socketSrvc.on('error', function(error) {
                if(error.type == 'UnauthorizedError' || error.code == 'invalid_token')
                    socketSrvc.isAuthenticated = false;

                deferred.reject()
            })

            socketSrvc.emit('authenticate', { token: CurrentUserService.token() })
        })

        return deferred.promise
    }

	socketSrvc.on = function(eventName, callback) {
		socketSrvc.socket.on(eventName, function(){
			var args = arguments;

			$rootScope.$apply(function(){
				callback.apply(socketSrvc.socket, args);
			})
		})
	}

	socketSrvc.emit = function(eventName, data) {
		var deferred = $q.defer()

		if(!socketSrvc.isAuthenticated) deferred.reject('socket not connected')

      	socketSrvc.socket.emit(eventName, data, function(err){
        	$rootScope.$apply(function(){
        		if(err) deferred.reject(err)
        		else deferred.resolve()
        	})
      	})

	    return deferred.promise
    }

    socketSrvc.sendPrivateMessage = function(messageOpts){
    	var sendSocketMessage = $q.defer()

    	var messageObj = {
    		sender: messageOpts.user_id,
    		message: messageOpts.message,
            receipient: messageOpts.partner_id,
            relationship_id: messageOpts.relationship_id
    	}

    	socketSrvc.emit('privateMessage', messageObj)
    		.then(sendSocketMessage.resolve)
    		.catch(sendSocketMessage.reject)

    	return sendSocketMessage.promise
    }

    // socketSrvc.joinRoom = function(roomId){
    //     if(!roomId) return;
    //     return socketSrvc.emit('join room', roomId)
    // }

    return socketSrvc;
}

