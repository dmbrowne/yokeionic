angular
	.module('accounts', ['ionic', 'cameraModule'])
	.config(function($stateProvider, $urlRouterProvider) {
		$stateProvider.state('tab.account', {
			url: '/account',
			views: {
				'tab-account': {
					templateUrl: 'account/tab-account.html',
					controller: 'AccountCtrl'
				}
			}
		})
	})
	.controller('AccountCtrl', function($scope, Camera) {
		$scope.settings = {
			enableFriends: true
		};

		$scope.picture = function() {
			Camera.getPicture()
		}
	});
