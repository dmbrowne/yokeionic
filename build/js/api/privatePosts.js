
angular
	.module('api')
	.service('PrivatePostsService', PrivatePosts);

PrivatePosts.$inject = ['$resource', 'apiconfig'];
function PrivatePosts($resource, apiconfig){
	return{
		privatePost: function(){
			return $resource( apiconfig.apiUrl + '/private-posts/:relationship_id/:post_id')
		}
	}
}
