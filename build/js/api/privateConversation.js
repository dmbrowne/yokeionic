
angular
	.module('api')
	.service('PrivateConversationService', PrivateConversation);

PrivateConversation.$inject = ['$resource', 'apiconfig'];
function PrivateConversation($resource, apiconfig){

	this.conversationByRelationshipId = function(){
		return $resource( apiconfig.apiUrl + '/private-conversations/relationship/:id' )
	}

	this.convoById = function(){
		return $resource( apiconfig.apiUrl + '/private-conversations/:id' )
	}

		// Not currently implemented by server
		// conversationById(){
		// 	return $resource( apiconfig.apiUrl + '/conversations/id/:id' )
		// }
}
