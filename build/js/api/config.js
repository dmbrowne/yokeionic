// (function(angular){

	angular
		.module('api')
		.factory('apiconfig', apiConfig)


	function apiConfig(){
		var ApiConfig      = {}

		ApiConfig.protocol = 'http://'
		ApiConfig.domain   = '192.168.0.2:3000'
		ApiConfig.basePath = '/api/v1'
		ApiConfig.apiUrl   = ApiConfig.protocol + ApiConfig.domain + ApiConfig.basePath;

		return ApiConfig
	}

// })(window.angular)
