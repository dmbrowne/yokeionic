angular
	.module('api')
	.service('UserService', UserService);

UserService.$inject = ['$resource', 'apiconfig'];
function UserService ($resource, apiconfig){

	this.users = $resource( apiconfig.apiUrl + '/users/:user_id' )

	this.getToken = function(userDetails){
		return $resource( apiconfig.apiUrl + '/login' ).save(userDetails).$promise
	}

	this.verify = function(){
		return $resource( apiconfig.apiUrl + '/verifyuser/:user_id', {}, {
			'update': { method: 'PUT'}
		})
	}
}
